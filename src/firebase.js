import firebase from 'firebase/app';
import 'firebase/database';
const config = {
  apiKey: "AIzaSyAUdrP4tJl2T_aZ7GMe0BWXKLbP_9AdxPM",
  authDomain: "reactjscomments.firebaseapp.com",
  databaseURL: "https://reactjscomments.firebaseio.com",
  projectId: "reactjscomments",
  storageBucket: "reactjscomments.appspot.com",
  messagingSenderId: "1012053559333"
};
firebase.initializeApp(config);

export const database = firebase.database();
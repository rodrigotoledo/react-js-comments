import React, { Component } from 'react'
export default class  extends Component {
  state = { newComment: '' }

  handleValue = event => {
    this.setState(
      {
        newComment: event.target.value
      }
    )
  }

  sendComment = () => {
    this.props.sendComment(this.state.newComment);
    this.setState({
      newComment: ''
    })
  }
  
  render() {
    return (
      <div>
        <textarea value={this.state.newComment} onChange={this.handleValue}></textarea>
        <button onClick={this.sendComment}>Enviar</button>
      </div>
    )
  }
}